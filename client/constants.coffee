module.exports = 
  CREATE_LOGIN_FORM: 'create login form'
  UPDATE_LOGIN_FORM: 'update login form'
  SUBMIT_LOGIN_FORM: 'submit login form'
  RESET_ERRORS_LOGIN_FORM: 'reset errors login form'

  LOGIN_CURRENT_USER: 'login current user'
  LOGIN_CURRENT_USER_SUCCESS: 'login current user success'
  LOGIN_CURRENT_USER_ERROR: 'login current user error'

  LOGOUT_CURRENT_USER: 'logout current user'
  LOGOUT_CURRENT_USER_SUCCESS: 'logout current user success'
  LOGOUT_CURRENT_USER_ERROR: 'logout current user error'

  FETCH_CURRENT_USER: 'fetch current user'
  FETCH_CURRENT_USER_SUCCESS: 'fetch current user success'
  FETCH_CURRENT_USER_ERROR: 'fetch current user error'

  REG_CURRENT_USER: 'register current user'
  REG_CURRENT_USER_SUCCESS: 'register current user success'
  REG_CURRENT_USER_ERROR: 'register current user error'

  FETCH_TASK_CATEGORY_LIST: 'fetch task category list'
  FETCH_TASK_CATEGORY_LIST_SUCCESS: 'fetch task category list success'
  FETCH_TASK_CATEGORY_LIST_ERROR: 'fetch task category list error'
  
  CREATE_TASK_DRAFT: 'create task draft'
  UPDATE_TASK_DRAFT: 'update task draft'

  ADD_TASK: 'add task'
  ADD_TASK_SUCCESS: 'add task success'
  ADD_TASK_ERROR: 'add task error'

  FETCH_TASK: 'fetch task'
  FETCH_TASK_SUCCESS: 'fetch task success'
  FETCH_TASK_ERROR: 'fetch task error'

  DELETE_TASK: 'delete task'
  DELETE_TASK_SUCCESS: 'delete task success'
  DELETE_TASK_ERROR: 'delete task error'

  UPDATE_TASK: 'update task'
  UPDATE_TASK_SUCCESS: 'update task success'
  UPDATE_TASK_ERROR: 'update task error'

  FETCH_TASK_LIST: 'fetch task list'
  FETCH_TASK_LIST_SUCCESS: 'fetch task list success'
  FETCH_TASK_LIST_ERROR: 'fetch task list error'

  REQUEST_TASK: 'request task success'
  REQUEST_TASK_SUCCESS: 'request task success success'
  REQUEST_TASK_ERROR: 'request task success error'

  ACCEPT_TASK: 'accept task success'
  ACCEPT_TASK_SUCCESS: 'accept task success success'
  ACCEPT_TASK_ERROR: 'accept task success error'

  REFUSE_TASK: 'refuse task success'
  REFUSE_TASK_SUCCESS: 'refuse task success success'
  REFUSE_TASK_ERROR: 'refuse task success error'

  COMPLETE_TASK: 'complete task'
  COMPLETE_TASK_SUCCESS: 'complete task success'
  COMPLETE_TASK_ERROR: 'complete task error'

  REJECT_DECLARE_TASK: 'reject delcare task'
  REJECT_DECLARE_TASK_SUCCESS: 'reject delcare task success'
  REJECT_DECLARE_TASK_ERROR: 'reject delcare task error'

  UPDATE_REG_FORM: 'update reg form'

  CREATE_PROFILE_FORM: 'create profile form'
  UPDATE_PROFILE_FORM: 'update profile form'

  UPDATE_CURRENT_USER: 'update current user'
  UPDATE_CURRENT_USER_SUCCESS: 'update current user success'
  UPDATE_CURRENT_USER_ERROR: 'update current user error'

  UPDATE_SNACKBAR: 'update snackbar'
  SHOW_SNACKBAR: 'show snackbar'
  HIDE_SNACKBAR: 'hide snackbar'

  CLEAR_TASK_PAGE: 'clear task page'

  FETCH_TASK_PAGE: 'description'
  FETCH_TASK_PAGE_SUCCESS: 'description success'
  FETCH_TASK_PAGE_ERROR: 'description error'

  #comment form
  UPDATE_COMMENT_FORM: 'update comment form'

  SUBMIT_COMMENT_FORM: 'submit comment form'
  SUBMIT_COMMENT_FORM_SUCCESS: 'submit comment form success'
  SUBMIT_COMMENT_FORM_ERROR: 'submit comment form error'
