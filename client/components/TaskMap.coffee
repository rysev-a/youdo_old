React = require 'react'
{Container} = require 'flux/utils'
CurrentUserStore = require '../stores/CurrentUserStore'

{div, a, p, form, input, label, h4} = React.DOM

class TaskMap extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  checkAuthorization:->
    if not @state.currentUser.is_active
      @context.router.push('/home/login-form')

  componentDidMount:->
    @checkAuthorization()

  componentDidUpdate:->
    @checkAuthorization()

  render:->
    div
      className: 'task-map'
      h4 {}, 'Карта с заданиями'

TaskMap.getStores = -> [CurrentUserStore]
TaskMap.calculateState = ->
  currentUser: CurrentUserStore.getState()

module.exports = Container.create(TaskMap)
