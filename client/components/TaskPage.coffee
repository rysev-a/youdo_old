React = require 'react'
update = require 'react-addons-update'
{Container} = require 'flux/utils'
{div, span, a, p, h4} = React.DOM

CurrentUserStore = require '../stores/CurrentUserStore'
TaskActionCreators = require '../actions/TaskActionCreators'
TaskPageActionCreators = require '../actions/TaskPageActionCreators'
TaskPageStore = require '../stores/TaskPageStore'
RequestList = require './profile/RequestList'
CommentForm = require './forms/CommentForm'
{Link} = require 'react-router'
Link = React.createFactory(Link)


class TaskPage extends React.Component
  constructor: ({params})->
    {@task_id} = params
  
  @contextTypes:
    router: React.PropTypes.object

  componentDidMount:->
    callback = ()=> TaskPageActionCreators.fetch(@task_id)
    setTimeout(callback, 0)
    @checkAuthorization()

  componentDidUpdate:->
    @checkAuthorization()

  checkAuthorization:->
    if not @state.currentUser.is_active
      @context.router.push('/home')

  componentWillUnmount: ->
    TaskPageActionCreators.clear()

  requestTask: (taskId, executorId)->
    if not @state.currentUser.is_active
      @context.router.push('home/registration-form')
      return false
    TaskActionCreators.requestTask(taskId, executorId)
    @context.router.push('profile/declared')

  getStatus: (task)->
    #if user is anonymous, return starndar status
    if not @state.currentUser.is_active
      return task.status

    status = task.status
    if Number(task.customer_id) is @state.currentUser.id
      status = 'Вы создали эту задачу'
    if (task.declared.filter (user)=> user.id is @state.currentUser.id).length
      status = 'Вы подали заявку на выполнение задачи'
    if Number(task.executor_id) is @state.currentUser.id
      status = 'Вы выполняете задачу'
    div className: 'task-status',
      status, ' '
      Link
        to: "/edit-task/#{task.id}"
        '(Редактировать)'

  comments:->
    getAuthor = (comment)->
      span
        className: 'task-comment__author__value'
        comment.user.first_name or comment.user.email

    task = @state.task

    div className: 'task-page-comments__wrapper',
      if task.comments.length
        div className: 'task-page-comments',
          div className: 'task-page-comments__title', 'Комментарии'
          task.comments.map (comment) ->
            div className: 'task-comment', key: comment.id,
              div className: 'task-comment__content', comment.content
              div className: 'task-comment__author', 'Автор: ', getAuthor(comment)
      React.createElement(CommentForm, task: task)
              
  render:->
    task = @state.task
    if task.loading
      div {}, 'загружаем задачу'
    else
      div
        className: 'task-page'
        h4 {}, task.name
        div className: 'row',
          div className: 'six columns',
            div className: 'task-page-description',
              div className: 'task-page-description__title', 'Описание'
              div className: 'task-page-description__value', task.description

            div className: 'task-page-customer',
              span className: 'task-page-customer__title', 'Заказчик: '
              span className: 'task-page-customer__value',
                 task.customer.first_name or task.customer.email
            div className: 'task-page-price',
              span className: 'task-page-price__title', 'Цена: '
              span 
                className: 'task-page-price__value', "#{task.price}"
                span className: 'task-page-price__value-valute', ' Рублей'
          div className: 'six columns',
            div className: 'task-page-info',
              div className: 'task-page-info__title', 'Дополнительная информация'
        div className: 'task-page-buttons',
          if @getStatus(task) is task.status
            a
              className: 'button button-primary'
              onClick: @requestTask.bind(this,
                    task.id, @state.currentUser.id)
              'Выполнить'
          else
            @getStatus(task)

        @comments()

TaskPage.getStores = -> [CurrentUserStore, TaskPageStore]
TaskPage.calculateState = ->
  currentUser: CurrentUserStore.getState()
  task: TaskPageStore.getState()
  
module.exports = Container.create(TaskPage)     
