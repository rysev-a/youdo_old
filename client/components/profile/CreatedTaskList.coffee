React = require 'react'
update = require 'react-addons-update'
{Container} = require 'flux/utils'

CurrentUserStore = require '../../stores/CurrentUserStore'
TaskActionCreators = require '../../actions/TaskActionCreators'
translateManager = require '../../utils/translateManager'

{div, a, p,
table, tbody, thead, th, tr, td} = React.DOM

class CreatedTaskList extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  render:->
    div
      className: 'created-task-list'
      'Созданные задачи'
      table className: 'created-task-table',
        thead {},
          tr {},
            th {}, 'Название'
            th {}, 'Описание'
            th {}, 'Статус'
            th {}, 'Удалить'
        tbody {},
          @state.currentUser.created_tasks.map (task, taskIndex)=>
            tr {key: taskIndex},
              td {}, 
                a
                  className: 'created-task__link'
                  onClick: ()=> @context.router.push("/edit-task/#{task.id}")
                  task.name
              td {}, task.description
              td {}, translateManager.translateStatus(task.status)
              td {},
                if task.status is 'waiting'
                  a
                    className: 'button'
                    onClick: TaskActionCreators.deleteTask.bind(this, task.id, taskIndex)
                    'Удалить'


CreatedTaskList.getStores = -> [CurrentUserStore]
CreatedTaskList.calculateState = ->
  currentUser: CurrentUserStore.getState()

module.exports = Container.create(CreatedTaskList)
