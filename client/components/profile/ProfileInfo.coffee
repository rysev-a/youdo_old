React = require 'react'
update = require 'react-addons-update'
{Container} = require 'flux/utils'
{div, a, p} = React.DOM

ProfileForm = require '../forms/ProfileForm'

class ProfileInfo extends React.Component
  render:->
    div
      className: 'profile-info'
      'Информация о профиле'
      React.createElement(ProfileForm)

module.exports = ProfileInfo
