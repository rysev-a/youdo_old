React = require 'react'
update = require 'react-addons-update'
{Container} = require 'flux/utils'
{Link} = require 'react-router'
Link = React.createFactory(Link)

CurrentUserStore = require '../../stores/CurrentUserStore'
TaskActionCreators = require '../../actions/TaskActionCreators'
translateManager = require '../../utils/translateManager'

{div, a, p,
table, tbody, thead, th, tr, td} = React.DOM

class ExecutedTaskList extends React.Component
  @contextTypes:
    router: React.PropTypes.object
  render:->
    div
      className: 'execured-task-list'
      'Выполняемые задачи'
      table className: 'execured-task-table',
        thead {},
          tr {},
            th {}, 'Название'
            th {}, 'Описание'
            th {}, 'Статус'
            th {}, 'Завершить'
        tbody {},
          if @state.currentUser.executed_tasks
            @state.currentUser.executed_tasks.map (task, taskIndex)=>
              tr {key: task.id},
                td {},
                  Link
                    to: "tasks/#{task.id}"
                    task.name
                td {}, task.description
                td {}, translateManager.translateStatus(task.status)
                td {},
                  if task.status is 'accepted'
                    a
                      className: 'button'
                      onClick: TaskActionCreators.completeTask.bind(this, task.id, taskIndex)
                      'Завершить'


ExecutedTaskList.getStores = -> [CurrentUserStore]
ExecutedTaskList.calculateState = ->
  currentUser: CurrentUserStore.getState()

module.exports = Container.create(ExecutedTaskList)
