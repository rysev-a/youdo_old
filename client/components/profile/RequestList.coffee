React = require 'react'
{Container} = require 'flux/utils'
{div, span, p, a,
table, tbody, thead, tr, th, td} = React.DOM

TaskActionCreators = require '../../actions/TaskActionCreators'

class RequestList extends React.Component
  constructor: (props)->
    @props = props

  @contextTypes:
    router: React.PropTypes.object

  accept: (taskId, userId)->
    TaskActionCreators.acceptTask(taskId, userId)
    @context.router.push('/profile/created')

  render:->
    table
      className: 'requested-list'
      thead {},
        tr {},
          th {}, 'Пользователь'
          th {}, 'Принять'

      tbody {},
        if @props.task.declared
          @props.task.declared.map (user)=>
            tr
              key: user.id
              className: 'requested-list__item'
              td {}, user.email
              td {},
                a
                  className: 'button button-primary'
                  onClick: @accept.bind(this, @props.task.id, user.id)
                  'Принять'

module.exports = RequestList
