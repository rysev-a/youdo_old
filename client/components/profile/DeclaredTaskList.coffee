React = require 'react'
update = require 'react-addons-update'
{Container} = require 'flux/utils'
{Link} = require 'react-router'
Link = React.createFactory(Link)

CurrentUserStore = require '../../stores/CurrentUserStore'
TaskActionCreators = require '../../actions/TaskActionCreators'
translateManager = require '../../utils/translateManager'

{div, a, p,
table, tbody, thead, th, tr, td} = React.DOM

class DeclaredTaskList extends React.Component
  @contextTypes:
    router: React.PropTypes.object
  render:->
    div
      className: 'declared-task-list'
      'Выполняемые задачи'
      table className: 'declared-task-table',
        thead {},
          tr {},
            th {}, 'Название'
            th {}, 'Описание'
            th {}, 'Статус'
            th {}, 'Отказаться'
        tbody {},
          @state.currentUser.declarers.map (task, taskIndex)=>
            tr {key: task.id},
              td {},
                Link
                  to: "tasks/#{task.id}"
                  task.name
              td {}, task.description
              td {}, translateManager.translateStatus(task.status)
              td {},
                a
                  className: 'button'
                  onClick: ()=>
                    TaskActionCreators.rejectDeclare(task.id, 
                      taskIndex, @state.currentUser.id)
                  'Отказаться'


DeclaredTaskList.getStores = -> [CurrentUserStore]
DeclaredTaskList.calculateState = ->
  currentUser: CurrentUserStore.getState()

module.exports = Container.create(DeclaredTaskList)
