React = require 'react'
{Container} = require 'flux/utils'
SnackBarStore = require '../../stores/SnackBarStore'

{div, span, a, p} = React.DOM




class SnackBar extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  getClass:->
    if @state.snackbar.show then 'visible' else 'hidden'
  render:->
    div
      className: "snackbar #{@getClass()}"
      div
        className: "snackbar-content  #{@state.snackbar.status}"
        div 
          className: 'snackbar-message'
          @state.snackbar.message


SnackBar.getStores = -> [SnackBarStore]
SnackBar.calculateState = (prevState) ->
  snackbar: SnackBarStore.getState()

module.exports = Container.create(SnackBar)
