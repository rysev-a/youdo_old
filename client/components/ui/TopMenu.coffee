React = require 'react'
{Link} = require 'react-router'
{Container} = require 'flux/utils'
CurrentUserActionCreators = require '../../actions/CurrentUserActionCreators'
CurrentUserStore = require '../../stores/CurrentUserStore'

{div, span, a, p} = React.DOM


# translate component to factory
Link = React.createFactory(Link)

class TopMenu extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  render:->
    div
      className: 'top-menu'
      Link 
        to: '/home'
        className: 'top-menu__item'
        activeClassName: 'active'          
        'Главная'
      if @state.currentUser.is_active
        span {},
          Link 
            to: '/profile'
            className: 'top-menu__item'
            activeClassName: 'active'          
            'Профиль'
          Link 
            to: "/create-task"
            className: 'top-menu__item'
            activeClassName: 'active'
            'Добавить задание'
      Link
        to: "/tasks"
        className: 'top-menu__item'
        activeClassName: 'active'
        'Найти задание'
      if @state.currentUser.is_active
        a
          className: 'top-menu__item'
          onClick: CurrentUserActionCreators.logoutCurrentUser
          'Выйти'          


TopMenu.getStores = -> [CurrentUserStore]
TopMenu.calculateState = (prevState) ->
  currentUser: CurrentUserStore.getState()

module.exports = Container.create(TopMenu)
