React = require 'react'
{Container} = require 'flux/utils'
{Link} = require 'react-router'
Link = React.createFactory(Link)

{div, span, a, p, h4,
table, tbody, thead, tr, th, td} = React.DOM

CurrentUserStore = require '../stores/CurrentUserStore'
TaskListStore = require '../stores/TaskListStore'
TaskListActionCreators = require '../actions/TaskListActionCreators'
TaskActionCreators = require '../actions/TaskActionCreators'


class TaskList extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  getStatus: (task)->
    #if user is anonymous, return starndar status
    if not @state.currentUser.is_active
      return task.status

    status = task.status
    if Number(task.customer_id) is @state.currentUser.id
      status = 'Вы создали'
    if (task.declared.filter (user)=> user.id is @state.currentUser.id).length
      status = 'Вы подали заявку'
    if Number(task.executor_id) is @state.currentUser.id
      status = 'Вы выполняете задачу'
    status

  componentDidMount:->
    setTimeout(TaskListActionCreators.fetchTaskList, 0)

  requestTask: (taskId, executorId)->
    if not @state.currentUser.is_active
      @context.router.push('home/registration-form')
      return false
    TaskActionCreators.requestTask(taskId, executorId)
    @context.router.push('profile/declared')

  render:->
    div
      className: 'task-list-page'
      h4 {}, 'Список задач'

      table className: 'task-list-table',
        thead {},
          tr {},
            th {}, 'Название'
            th {}, 'Описание'
            th {}, 'Цена'
            th {}, 'Выполнить'
        tbody {},
          @state.taskList.map (task, taskIndex)=>
            tr {key: task.id},
              td {},
                Link
                  to: "tasks/#{task.id}"
                  task.name
              td {}, task.description
              td {}, task.price
              td {},
                if @getStatus(task) is task.status
                  a 
                    className: 'button'
                    onClick: @requestTask.bind(this, 
                      task.id, @state.currentUser.id)
                    'Выполнить'
                else
                  span {}, @getStatus(task)

TaskList.getStores = -> [CurrentUserStore, TaskListStore]
TaskList.calculateState = ->
  currentUser: CurrentUserStore.getState()
  taskList: TaskListStore.getState()
  currentPage: 0

module.exports = Container.create(TaskList)
