React = require 'react'
{Link} = require 'react-router'
{Container} = require 'flux/utils'
CurrentUserStore = require '../stores/CurrentUserStore'
CurrentUserActionCreators = require '../actions/CurrentUserActionCreators'

{div, a, p, h4} = React.DOM
Link = React.createFactory(Link)

class Home extends React.Component
  @contextTypes:
    router: React.PropTypes.object
    
  render:->
    div
      className: 'home-page'
      if not @state.currentUser.is_active
        div className: 'start-forms',
          h4 {}, 'Войти или Зарегистрироваться'
          div
            className: 'tab-nav'
            Link 
              to: '/home/login-form'
              className: 'tab-nav__item button'
              activeClassName: 'active'          
              'Войти'
            Link 
              to: '/home/registration-form'
              className: 'tab-nav__item button'
              activeClassName: 'active'          
              'Зарегистрироваться'        
      div
        className: 'tab-content'
        @props.children


Home.getStores = -> [CurrentUserStore]
Home.calculateState = (prevState) ->
  currentUser: CurrentUserStore.getState()

module.exports = Container.create(Home)   
