React = require 'react'
{Container} = require 'flux/utils'
{Link} = require 'react-router'
Link = React.createFactory(Link)

{div, a, p, h4} = React.DOM

CurrentUserStore = require '../stores/CurrentUserStore'

class ProfileLink extends React.Component
  constructor: ({@target, @title})->
  render:->
    Link 
      to: @target
      className: 'tab-nav__item button'
      activeClassName: 'active'
      @title

ProfileLink = React.createFactory(ProfileLink)

class Profile extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  checkAuthorization:->
    if not @state.currentUser.is_active
      @context.router.push('/home')

  componentDidMount:->
    @checkAuthorization()

  componentDidUpdate:->
    @checkAuthorization()

  render:->
    div
      className: 'profile-page'
      h4 {}, 'Профиль'


      div
        className: 'tab-nav'
        ProfileLink 
          target: '/profile/info', title: 'Инфорация'
        ProfileLink 
          target: '/profile/created', title: 'Созданные задачи'
        ProfileLink 
          target: '/profile/executed', title: 'Исполняемые задачи'        
        ProfileLink 
          target: '/profile/declared', title: 'Запросы'

      div
        className: 'tab-content'
        if @state.currentUser.is_active then @props.children else div {}

Profile.getStores = -> [CurrentUserStore]
Profile.calculateState = ->
  currentUser: CurrentUserStore.getState()

module.exports = Container.create(Profile)
