React = require 'react'
{Container} = require 'flux/utils'
update = require 'react-addons-update'
{div, a, p, h4} = React.DOM

CurrentUserStore = require '../stores/CurrentUserStore'
TaskDraftStore = require '../stores/TaskDraftStore'
TaskActionCreators = require '../actions/TaskActionCreators'


TaskForm = require './forms/TaskForm'

class TaskCreate extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  checkAuthorization:->
    if not @state.currentUser.is_active
      @context.router.push('/home')

  componentDidMount:->
    setTimeout(TaskActionCreators.createTaskDraft, 0)
    @checkAuthorization()

  componentDidUpdate:->
    @checkAuthorization()

  handleChange: (field, value)->
    TaskActionCreators.updateTaskDraft(field, value)

  handleSubmit:->
    draft = update(@state.draft, customerId: 
      $set: @state.currentUser.id)

    TaskActionCreators.addTask(draft)
    @context.router.push('/profile/created')

  render:->
    div
      className: 'new-task-page'
      h4 {}, 'Добавить задание'
      if @state.draft.loading is false
        React.createElement TaskForm,
          draft: @state.draft
          buttonLabel: 'Создать задачу'
          handleChange: @handleChange.bind(this)
          handleSubmit: @handleSubmit.bind(this)

TaskCreate.getStores = -> [CurrentUserStore, TaskDraftStore]
TaskCreate.calculateState = ->
  currentUser: CurrentUserStore.getState()
  draft: TaskDraftStore.getState()
  
module.exports = Container.create(TaskCreate)     
