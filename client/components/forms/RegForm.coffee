React = require 'react'
{Container} = require 'flux/utils'

RegFormActionCreators = require '../../actions/forms/RegFormActionCreators'
RegFormStore = require '../../stores/forms/RegFormStore'
CurrentUserStore = require '../../stores/CurrentUserStore'
SnackBarActionCreators = require '../../actions/SnackBarActionCreators'

{div, a, p, form, input, label} = React.DOM

class RegForm extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  checkAuthorization:->
    if @state.currentUser.is_active
      @context.router.push('/home')

  componentDidMount:->
    @checkAuthorization()

  componentDidUpdate:->
    @checkAuthorization()

  handleChange: (field, e)->
    RegFormActionCreators.updateRegForm(field, e.target.value)

  handleSubmit:->
    RegFormActionCreators.submitRegForm
      email: @state.formData.email
      password: @state.formData.password

    SnackBarActionCreators.show()

  render:->
    form
      className: 'reg-form'
      label {}, 'Ваш email'
      input
        type: 'email'
        placeholder: 'user@email.com'
        onChange: @handleChange.bind(this, 'email')
      label {}, 'Пароль'
      input
        type: 'password'
        placeholder: '******'
        onChange: @handleChange.bind(this, 'password')
      div className: 'reg-form__buttons',
        a
          className: 'button button-primary'
          onClick: @handleSubmit.bind(this)
          'Зарегистрироваться'

RegForm.getStores = -> [RegFormStore, CurrentUserStore]
RegForm.calculateState = (prevState) ->
  formData: RegFormStore.getState()
  currentUser: CurrentUserStore.getState()


module.exports = Container.create(RegForm)   
