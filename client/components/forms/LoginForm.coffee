React = require 'react'
{Container} = require 'flux/utils'

SnackBarActionCreators = require '../../actions/SnackBarActionCreators'
LoginFormActionCreators = require '../../actions/forms/LoginFormActionCreators'
LoginFormStore = require '../../stores/forms/LoginFormStore'
CurrentUserStore = require '../../stores/CurrentUserStore'


{div, a, p, form, input, label} = React.DOM

class LoginForm extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  checkAuthorization:->
    if @state.currentUser.is_active
      @context.router.push('/home')

  componentDidMount:->
    @checkAuthorization()

  componentDidUpdate:->
    @checkAuthorization()

  handleChange: (field, e)->
    LoginFormActionCreators.updateLoginForm(field, e.target.value)

  handleSubmit:->
    LoginFormActionCreators.submitLoginForm
      email: @state.formData.email
      password: @state.formData.password

    SnackBarActionCreators.show()

  render:->
    form
      className: 'login-form'
      label {}, 'Ваш email'
      input
        type: 'email'
        placeholder: 'user@email.com'
        onChange: @handleChange.bind(this, 'email')
      label {}, 'Пароль'
      input
        type: 'password'
        onChange: @handleChange.bind(this, 'password')
      div className: 'login-form__buttons',
        a
          className: 'button button-primary'
          onClick: @handleSubmit.bind(this)
          'Войти'

LoginForm.getStores = -> [LoginFormStore, CurrentUserStore]
LoginForm.calculateState = (prevState) ->
  formData: LoginFormStore.getState()
  currentUser: CurrentUserStore.getState()


module.exports = Container.create(LoginForm)   
