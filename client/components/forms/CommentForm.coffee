React = require 'react'
{Container} = require 'flux/utils'

CommentFormActionCreators = require '../../actions/forms/CommentFormActionCreators'
CommentFormStore = require '../../stores/forms/CommentFormStore'
CurrentUserStore = require '../../stores/CurrentUserStore'


{div, a, p, form, input, label, textarea} = React.DOM

class CommentForm extends React.Component
  constructor: (props)->
    super props
    @props = props
  handleChange: (field, e)->
    CommentFormActionCreators.updateCommentForm(field, e.target.value)

  handleSubmit:->
    CommentFormActionCreators.submitCommentForm
      user_id: @state.currentUser.id
      task_id: @props.task.id
      content: @state.formData.content

  render:->
    form
      className: 'comment-form'
      textarea
        value: @state.formData.content
        onChange: @handleChange.bind(this, 'content')

      div className: 'comment-form__buttons',
        a
          className: 'button button-primary'
          onClick: @handleSubmit.bind(this)
          'Отправить'

CommentForm.getStores = -> [CommentFormStore, CurrentUserStore]
CommentForm.calculateState = (prevState) ->
  formData: CommentFormStore.getState()
  currentUser: CurrentUserStore.getState()


module.exports = Container.create(CommentForm)   
