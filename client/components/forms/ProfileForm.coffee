React = require 'react'
{Container} = require 'flux/utils'

SnackBarActionCreators = require '../../actions/SnackBarActionCreators'
ProfileFormActionCreators = require '../../actions/forms/ProfileFormActionCreators'
ProfileFormStore = require '../../stores/forms/ProfileFormStore'
CurrentUserStore = require '../../stores/CurrentUserStore'


{div, a, p, form, input, label} = React.DOM

class ProfileForm extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  componentDidMount:->
    setTimeout(ProfileFormActionCreators.createForm, 0)

  componentWillUnmount:->
    callback = ProfileFormActionCreators.update
    setTimeout(callback.bind(this, 'loading', true), 0)

  handleChange: (field, e)->
    ProfileFormActionCreators.update(field, e.target.value)

  submit:->
    userId = @state.currentUser.id
    formData = @state.formData
    ProfileFormActionCreators.submit(userId, formData)

    SnackBarActionCreators.show()

  render:->
    if @state.formData.loading
      return div {}, 'Загружаются данные пользователя'

    form
      className: 'profile-form'
      label {}, 'Электронная почта'
      input
        defaultValue: @state.formData.email
        type: 'email'
        placeholder: 'user@email.com'
        onChange: @handleChange.bind(this, 'email')
      label {}, 'Имя'
      input
        defaultValue: @state.formData.first_name
        type: 'text'
        placeholder: 'Имя'
        onChange: @handleChange.bind(this, 'first_name')
      label {}, 'Фамилия'
      input
        defaultValue: @state.formData.last_name
        type: 'text'
        placeholder: 'Фамилия'
        onChange: @handleChange.bind(this, 'last_name')


      div className: 'profile-form__buttons',
        a
          className: 'button button-primary'
          onClick: @submit.bind(this)
          'Сохранить'

ProfileForm.getStores = -> [ProfileFormStore, CurrentUserStore]
ProfileForm.calculateState = (prevState) ->
  formData: ProfileFormStore.getState()
  currentUser: CurrentUserStore.getState()

module.exports = Container.create(ProfileForm)   
