React = require 'react'
{Container} = require 'flux/utils'
TaskCategoryListStore = require '../../stores/TaskCategoryListStore'


{form, input, textarea, select, label, option, div, span, p, a} = React.DOM

class TaskFrom extends React.Component
  constructor: (props)->
    @props = props

  handleChange: (field, e)->
    @props.handleChange(field, e.target.value)

  handleClose: (e)->
    e.preventDefault()
    @props.handleClose()

  componentWillUnmount:->
    @props.handleChange('loading', true)

  render:->
    form
      onSubmit: @props.handleSubmit.bind(this) 
      className: 'task-form'
      label {}, 'Название'
      input 
        className: 'u-full-width'
        defaultValue: @props.draft.name
        type: 'text'
        onChange: @handleChange.bind(this, 'name')
        placeholder: 'Название задачи'
        required: true
        autoFocus: true

      label {}, 'Описание'
      textarea
        className: 'u-full-width'
        defaultValue: @props.draft.description
        type: 'text'
        onChange: @handleChange.bind(this, 'description')
        placeholder: 'Описание задачи'
        required: true

      label {}, 'Категория'
      select
        className: 'u-full-width'
        onChange: @handleChange.bind(this, 'categoryId')
        defaultValue: if @props.draft.category then @props.draft.category.id
        option value: 0, 'Выбрать категорию'
        @state.taskCategoryList.map (category)->
          option
            key: category.id
            value: category.id
            category.name

      label {}, 'Цена в рублях'
      input 
        className: 'u-full-width'
        defaultValue: @props.draft.price
        type: 'text'
        onChange: @handleChange.bind(this, 'price')
        placeholder: '1000'
        required: true

      div
        className: 'task-form-buttons'
        a 
          className: 'button button-primary'
          onClick: @props.handleSubmit.bind(this)
          @props.buttonLabel


TaskFrom.getStores = -> [TaskCategoryListStore]
TaskFrom.calculateState = ->
  taskCategoryList: TaskCategoryListStore.getState()

module.exports = Container.create(TaskFrom)
