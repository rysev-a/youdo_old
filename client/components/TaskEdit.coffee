React = require 'react'
update = require 'react-addons-update'
{Container} = require 'flux/utils'
{div, a, p, h4} = React.DOM

CurrentUserStore = require '../stores/CurrentUserStore'
TaskDraftStore = require '../stores/TaskDraftStore'
TaskActionCreators = require '../actions/TaskActionCreators'

TaskForm = require './forms/TaskForm'
RequestList = require './profile/RequestList'


class TaskEdit extends React.Component
  constructor: ({params})->
    {@task_id} = params
  
  @contextTypes:
    router: React.PropTypes.object

  componentDidMount:->
    callback = ()=> TaskActionCreators.fetchTask(@task_id)
    setTimeout(callback, 10)
    @checkAuthorization()

  componentDidUpdate:->
    @checkAuthorization()

  checkAuthorization:->
    if not @state.currentUser.is_active
      @context.router.push('/home')

  handleChange: (field, value)->
    TaskActionCreators.updateTaskDraft(field, value)

  handleSubmit:->
    TaskActionCreators.updateTask(@state.draft)
    @context.router.push('/profile/created')

  render:->
    if @state.draft.loading
      div {}, 'загружаем задачу'
    div
      className: 'edit-task-page'
      h4 {}, @state.draft.name
      div className: 'row',
        div className: 'six columns',
          if @state.draft.loading is false
            React.createElement TaskForm,
              draft: @state.draft
              buttonLabel: 'Сохранить изменения'
              handleChange: @handleChange.bind(this)
              handleSubmit: @handleSubmit.bind(this)
        div className: 'six columns',
          div {}, 'Запросы'
          if @state.draft.loading is false
            React.createElement RequestList,
              task: @state.draft



TaskEdit.getStores = -> [CurrentUserStore, TaskDraftStore]
TaskEdit.calculateState = ->
  currentUser: CurrentUserStore.getState()
  draft: TaskDraftStore.getState()
  
module.exports = Container.create(TaskEdit)     
