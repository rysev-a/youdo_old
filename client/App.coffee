React = require 'react'
{render} = require 'react-dom'
{Container} = require 'flux/utils'
CurrentUserStore = require './stores/CurrentUserStore'
AppStore = require './stores/AppStore'
{Router, Route, browserHistory, 
 hashHistory, Redirect, IndexRoute, 
 DefaultRoute, Link} = require 'react-router'

#actions
CurrentUserActionCreators = require './actions/CurrentUserActionCreators'
TaskCategoryListActionCreators = require './actions/TaskCategoryListActionCreators'
TaskCategoryListStore = require './stores/TaskCategoryListStore'

# translate component to factory
[Route, 
 IndexRoute, 
 Link, 
 Router,
 Redirect] = [Route, 
              IndexRoute, 
              Link, 
              Router,
              Redirect].map (component)->
  component = React.createFactory component

TopMenu = require './components/ui/TopMenu'
SnackBar = require './components/ui/SnackBar'
Home = require './components/Home'
TaskMap = require './components/TaskMap'
LoginForm = require './components/forms/LoginForm'
RegForm = require './components/forms/RegForm'


#Task components
TaskEdit = require './components/TaskEdit'
TaskCreate = require './components/TaskCreate'
TaskList = require './components/TaskList'
TaskPage = require './components/TaskPage'

#Profile components
Profile = require './components/Profile'
ProfileInfo = require './components/profile/ProfileInfo'
CreatedTaskList = require './components/profile/CreatedTaskList'
ExecutedTaskList = require './components/profile/ExecutedTaskList'
DeclaredTaskList = require './components/profile/DeclaredTaskList'

{div, a, p} = React.DOM

class App extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  componentDidMount:->
    CurrentUserActionCreators.fetchCurrentUser()
    TaskCategoryListActionCreators.fetchTaskCategoryList()

  loadingClass:->
    if @state.app.loading
      'loading loading-wrapper'
    else
      'loading-wrapper'

  render: ->
    appContent = =>
      div className: 'app-content',
        React.createElement(TopMenu, routeParams: @props.routeParams)
        div
          className: 'content'
          @props.children

    div 
      className: 'container'
      if @state.app.profileLoaded then appContent() else div {}
      div className: @loadingClass(),
        div className: 'loading-img'
      React.createElement(SnackBar)


App.getStores = -> [CurrentUserStore, AppStore]
App.calculateState = ->
  currentUser: CurrentUserStore.getState()
  app: AppStore.getState()


router = Router
  'history': browserHistory
  Redirect from: '/', to: '/home'
  Redirect from: '/profile', to: '/profile/info'
  Route 'path': '/',  'component': Container.create(App),
    Route path: 'home', 'component': Home,
      Route path: 'login-form', 'component': LoginForm
      Route path: 'registration-form', 'component': RegForm
      IndexRoute 'component': TaskMap
    Route path: 'profile', 'component': Profile,
      Route path: 'info', component: ProfileInfo
      Route path: 'executed', component: ExecutedTaskList
      Route path: 'created', component: CreatedTaskList
      Route path: 'declared', component: DeclaredTaskList
    Route path: 'create-task', 'component': TaskCreate
    Route path: 'edit-task/:task_id', 'component': TaskEdit
    Route path: 'tasks', 'component': TaskList
    Route path: 'tasks/:task_id', 'component': TaskPage

render(router, document.getElementById('app'))
