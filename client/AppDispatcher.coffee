{Dispatcher} = require 'flux'


class AppDispatcher extends Dispatcher
  dispatchAsync: (promise, types, payload)->
    {request, success, failure} = types

    @dispatch
      type: request
      payload: Object.assign({}, payload)

    promise.then (response)=>
      type = if response.ok then success else failure
      response.json().then (responseData)=>
        @dispatch
          type: type
          payload: Object.assign({}, payload, response: responseData)


module.exports = new AppDispatcher()
