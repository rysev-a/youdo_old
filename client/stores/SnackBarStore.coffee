AppDispatcher = require '../AppDispatcher'
constants = require '../constants'
{ReduceStore} = require 'flux/utils'
update = require 'react-addons-update'

snackBarTimer = {}
showMessage = ->
  clearTimeout(snackBarTimer)
  AppDispatcher.dispatch
    type: constants.SHOW_SNACKBAR
    
  snackBarTimer = -> AppDispatcher.dispatch
    type: constants.HIDE_SNACKBAR
  setTimeout(snackBarTimer, 2000)

class SnackBarStore extends ReduceStore
  getInitialState: ->
    prevMessage: ''
    message: ''
    status: 'success'
    show: false

  notice: (message, type='success')->
    setTimeout(showMessage, 0)
    update @getState(), 
      message: $set: message
      status: $set: type

  reduce: (state, action)->
    switch (action.type)
      when constants.SHOW_SNACKBAR
        update(@getState(), show: $set: true)

      when constants.HIDE_SNACKBAR
        update(@getState(), show: $set: false)

      when constants.LOGIN_CURRENT_USER_SUCCESS
        @notice('Вы успешно вошли')

      when constants.LOGIN_CURRENT_USER_ERROR
        @notice('Ошибка входа', 'error')

      when constants.REG_CURRENT_USER_SUCCESS
        @notice('Вы успешно зарегистрированы')

      when constants.REG_CURRENT_USER_ERROR
        @notice('Ошибка регистрации', 'error')

      when constants.UPDATE_CURRENT_USER_SUCCESS
        @notice('Данные обновлены')

      when constants.UPDATE_CURRENT_USER_ERROR
        @notice('Ошибка обновления данных', 'error')

      else
        state

module.exports = new SnackBarStore(AppDispatcher)
