AppDispatcher = require '../AppDispatcher'
constants = require '../constants'
{ReduceStore} = require 'flux/utils'
update = require 'react-addons-update'

StartState = ->
  loading: true
  profileLoaded: false

class AppStore extends ReduceStore
  getInitialState: ->  StartState()
  reduce: (state, action)->
    if action.type in [
      constants.FETCH_TASK
      constants.FETCH_TASK_LIST
      constants.LOGIN_CURRENT_USER
      constants.UPDATE_CURRENT_USER
      constants.REG_CURRENT_USER
      constants.LOGOUT_CURRENT_USER]
      return update(@getState(), loading: $set: true)

    if action.type in [
      constants.FETCH_TASK_SUCCESS
      constants.FETCH_TASK_ERROR
      constants.FETCH_TASK_LIST_SUCCESS
      constants.FETCH_TASK_LIST_ERROR
      constants.UPDATE_CURRENT_USER_SUCCESS
      constants.UPDATE_CURRENT_USER_ERROR      
      constants.LOGIN_CURRENT_USER_SUCCESS
      constants.LOGIN_CURRENT_USER_ERROR
      constants.REG_CURRENT_USER_SUCCESS
      constants.REG_CURRENT_USER_ERROR
      constants.LOGOUT_CURRENT_USER_SUCCESS
      constants.LOGOUT_CURRENT_USER_ERROR]
      return update(@getState(), loading: $set: false)

    switch (action.type)
      when constants.FETCH_CURRENT_USER_SUCCESS
        update @getState(), 
          profileLoaded: $set: true
          loading: $set: false
      when constants.FETCH_CURRENT_USER_ERROR
        update @getState(), 
          profileLoaded: $set: true
          loading: $set: false
      else
        state

module.exports = new AppStore(AppDispatcher)
