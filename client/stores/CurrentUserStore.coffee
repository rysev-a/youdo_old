AppDispatcher = require '../AppDispatcher'
constants = require '../constants'
{ReduceStore} = require 'flux/utils'
update = require 'react-addons-update'

AnonymousUser = ->
    id: Date.now(),
    first_name: ''
    last_name: ''
    is_active: false
    email: ''


class CurrentUserStore extends ReduceStore
  getInitialState: ->  AnonymousUser()
  reduce: (state, action)->
    switch (action.type)
      when constants.FETCH_CURRENT_USER
        state
      when constants.FETCH_CURRENT_USER_SUCCESS
        action.payload.response
      when constants.FETCH_CURRENT_USER_ERROR
        new Error(action.type)
        state

      #login current user
      when constants.LOGIN_CURRENT_USER_SUCCESS
        action.payload.response

      #login current user
      when constants.REG_CURRENT_USER_SUCCESS
        action.payload.response

      #logout current user
      when constants.LOGOUT_CURRENT_USER_SUCCESS
        AnonymousUser()

      when constants.DELETE_TASK_SUCCESS
        taskIndex = action.payload.taskIndex
        update(@getState(), created_tasks: $splice: [[taskIndex, 1]])

      when constants.ADD_TASK_SUCCESS
        update(@getState(), created_tasks: $push: [action.payload.response])

      when constants.UPDATE_TASK_SUCCESS
        task = action.payload.response
        oldTask = (state.created_tasks.filter (t)=> t.id is task.id)[0]
        taskIndex = state.created_tasks.indexOf(oldTask)
        update(@getState(), created_tasks: $splice: [[taskIndex, 1, task]])

      when constants.REQUEST_TASK_SUCCESS
        update(@getState(), declarers: $push: [action.payload.response])

      when constants.REJECT_DECLARE_TASK_SUCCESS
        taskIndex = action.payload.taskIndex
        update(@getState(), declarers: $splice: [[taskIndex, 1]])

      when constants.ACCEPT_TASK_SUCCESS
        task = action.payload.response
        taskIndex = state.created_tasks.indexOf((state.created_tasks.filter (t)-> 
          t.id is task.id)[0])
        update(@getState(), created_tasks: $splice: [[taskIndex, 1, task]])

      when constants.REFUSE_TASK_SUCCESS
        taskIndex = action.payload.taskIndex
        task = action.payload.response
        update(@getState(), created_tasks: $splice: [[taskIndex, 1, task]])

      when constants.COMPLETE_TASK_SUCCESS
        taskIndex = action.payload.taskIndex
        update(@getState(), executed_tasks: $splice: [[taskIndex, 1, action.payload.response]]) 

      when constants.UPDATE_CURRENT_USER_SUCCESS
        action.payload.response
      else
        state

module.exports = new CurrentUserStore(AppDispatcher)
