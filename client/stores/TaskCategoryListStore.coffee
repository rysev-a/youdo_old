AppDispatcher = require '../AppDispatcher'
{ReduceStore} = require 'flux/utils'
update = require 'react-addons-update'

constants = require '../constants'

class TaskCategoryListStore extends ReduceStore
  getInitialState:->
    []
  reduce: (state, action)->
    switch action.type
      when constants.FETCH_TASK_CATEGORY_LIST
        state
      when constants.FETCH_TASK_CATEGORY_LIST_SUCCESS
        action.payload.response
      when constants.FETCH_TASK_CATEGORY_LIST_ERROR
        state
      else
        state

module.exports = new TaskCategoryListStore(AppDispatcher)
