AppDispatcher = require '../../AppDispatcher'
constants = require '../../constants'
{ReduceStore} = require 'flux/utils'
update = require 'react-addons-update'

defaultState = ->
    email: ''
    password: ''
    emailErrorMessage: ''
    passwordErrorMessage: ''
    loading: false


class RegFormStore extends ReduceStore
  getInitialState: ->  defaultState()
  reduce: (state, action)->
    switch (action.type)
      when constants.REG_CURRENT_USER
        update(@getState(), loading: $set: true)

      when constants.REG_CURRENT_USER_SUCCESS
        update(@getState(), loading: $set: false)

      when constants.REG_CURRENT_USER_ERROR
        update(@getState, loading: $set: false)

      when constants.UPDATE_REG_FORM
        update(@getState(), 
               "#{action.payload.field}": $set: action.payload.value)

      when constants.RESET_ERRORS_REG_FORM
        state

      else
        state

module.exports = new RegFormStore(AppDispatcher)
