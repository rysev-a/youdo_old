AppDispatcher = require '../../AppDispatcher'
constants = require '../../constants'
{ReduceStore} = require 'flux/utils'
update = require 'react-addons-update'

defaultState = ->
    content: 'Ваш комментарий'

class CommentFormStore extends ReduceStore
  getInitialState: ->  defaultState()
  reduce: (state, action)->
    switch (action.type)
      when constants.UPDATE_COMMENT_FORM
        update(@getState(), 
               "#{action.payload.field}": $set: action.payload.value)
      when constants.SUBMIT_COMMENT_FORM_SUCCESS
        update @getState(),
          content: $set: ''
      else
        state

module.exports = new CommentFormStore(AppDispatcher)
