AppDispatcher = require '../../AppDispatcher'
constants = require '../../constants'
{ReduceStore} = require 'flux/utils'
update = require 'react-addons-update'

defaultState = ->
    email: ''
    password: ''
    emailErrorMessage: ''
    passwordErrorMessage: ''
    loading: false


class LoginFormStore extends ReduceStore
  getInitialState: ->  defaultState()
  reduce: (state, action)->
    switch (action.type)
      when constants.LOGIN_CURRENT_USER
        update(@getState(), loading: $set: true)

      when constants.LOGIN_CURRENT_USER_SUCCESS
        update(@getState(), loading: $set: false)

      when constants.LOGIN_CURRENT_USER_ERROR
        update(@getState(), loading: $set: false)

      when constants.UPDATE_LOGIN_FORM
        update(@getState(), 
               "#{action.payload.field}": $set: action.payload.value)

      when constants.RESET_ERRORS_LOGIN_FORM
        state

      when constants.LOGOUT_CURRENT_USER_SUCCESS
        defaultState()

      else
        state

module.exports = new LoginFormStore(AppDispatcher)
