AppDispatcher = require '../../AppDispatcher'
constants = require '../../constants'
{ReduceStore} = require 'flux/utils'
update = require 'react-addons-update'

CurrentUserStore = require './../CurrentUserStore'

class ProfileFormStore extends ReduceStore
  getInitialState: ->  loading: true
  reduce: (state, action)->
    switch (action.type)
      when constants.CREATE_PROFILE_FORM
        userData = CurrentUserStore.getState()
        update(userData, loading: $set: false)

      when constants.UPDATE_PROFILE_FORM
        update(@getState(), 
               "#{action.payload.field}": $set: action.payload.value)

      else
        state

module.exports = new ProfileFormStore(AppDispatcher)
