AppDispatcher = require '../AppDispatcher'
{ReduceStore} = require 'flux/utils'
update = require 'react-addons-update'
constants = require '../constants'

defaultDraft = ->
  id: Date.now(),
  name: ''
  description: ''
  price: 0
  loading: false

class TaskDraft extends ReduceStore
  getInitialState: ->  {}
  reduce: (state, action)->   
    switch (action.type)
      when constants.CREATE_TASK_DRAFT
        return defaultDraft()

      when constants.UPDATE_TASK_DRAFT
        update(@getState(), 
               "#{action.payload.field}": $set: action.payload.value)

      when constants.FETCH_TASK
        update(@getState(), loading: $set: true)

      when constants.FETCH_TASK_SUCCESS
        update(action.payload.response, loading: $set: false)

      when constants.ADD_TASK_SUCCESS
        {}

      when constants.UPDATE_TASK_SUCCESS
        {}

      when constants.ACCEPT_TASK_SUCCESS
        Object.assign {}, state, action.payload.response

      else
        state

module.exports = new TaskDraft(AppDispatcher)
