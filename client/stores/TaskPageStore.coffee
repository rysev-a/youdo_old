AppDispatcher = require '../AppDispatcher'
{ReduceStore} = require 'flux/utils'
update = require 'react-addons-update'
constants = require '../constants'

class TaskPage extends ReduceStore
  getInitialState: ->  loading: true
  reduce: (state, action)->   
    switch (action.type)
      when constants.FETCH_TASK_PAGE_SUCCESS
        update(action.payload.response, loading: $set: false)
      when constants.CLEAR_TASK_PAGE
        update(@getState(), loading: $set: true)
      when constants.SUBMIT_COMMENT_FORM_SUCCESS
        update(@getState(),
          comments: $push: [action.payload.response])
      else
        state

module.exports = new TaskPage(AppDispatcher)
