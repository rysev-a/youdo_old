AppDispatcher = require '../AppDispatcher'
{ReduceStore} = require 'flux/utils'
update = require 'react-addons-update'

constants = require '../constants'

class TaskListStore extends ReduceStore
  getInitialState:->
    []
  reduce: (state, action)->
    switch action.type
      when constants.FETCH_TASK_LIST
        state
      when constants.FETCH_TASK_LIST_SUCCESS
        action.payload.response
      when constants.FETCH_TASK_LIST_ERROR
        new Error action.type
        state
      when constants.REQUEST_TASK_SUCCESS
        task = (state.filter (t) => t.id is action.payload.response.id)[0]
        taskIndex = state.indexOf(task)
        update(@getState(), $splice: [[taskIndex, 1]])
      else
        state


module.exports = new TaskListStore(AppDispatcher)
