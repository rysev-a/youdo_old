require 'whatwg-fetch'
serializer = require '../utils/serializer'

API_URL = '/api/v1'
API_HEADERS = 
  'Content-Type': 'application/json'

TaskAPI =
  # task list api
  fetchTaskList:->
    fetch("#{API_URL}/tasks")  

  # task category list api
  fetchTaskCategoryList:->
    fetch("#{API_URL}/task-categories")

  addTask: (taskData)->
    fetch "#{API_URL}/tasks",
      method: 'post'
      body: JSON.stringify(serializer.addTaskSerializer(taskData))
      headers: API_HEADERS


  # task api
  fetchTask: (taskId)->
    fetch("#{API_URL}/tasks/#{taskId}")
  
  updateTask: (taskData)->
    fetch "#{API_URL}/tasks/#{taskData.id}",
      method: 'put'
      body: JSON.stringify(serializer.updateTaskSerializer(taskData))
      headers: API_HEADERS

  requestTask: (taskId, userId)->
    fetch "#{API_URL}/tasks/request/#{taskId}",
      method: 'post'
      body: JSON.stringify({userId})
      headers: API_HEADERS

  rejectDeclare: (taskId, userId)->
    fetch "#{API_URL}/tasks/reject/#{taskId}",
      method: 'post'
      body: JSON.stringify({userId})
      headers: API_HEADERS

  acceptTask: (taskId, userId)->
    fetch "#{API_URL}/tasks/accept/#{taskId}",
      method: 'post'
      body: JSON.stringify({userId})
      headers: API_HEADERS

  deleteTask: (taskId)->
    fetch "#{API_URL}/tasks/#{taskId}",
      method: 'delete'

module.exports = TaskAPI
