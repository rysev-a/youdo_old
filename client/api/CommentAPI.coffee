require 'whatwg-fetch'
serializer = require '../utils/serializer'

API_URL = '/api/v1'
API_HEADERS = 
  'Content-Type': 'application/json'

CommentAPI =
  addComment: (data)->
    fetch "#{API_URL}/comments",
      method: 'post'
      body: JSON.stringify(data)
      headers: API_HEADERS

module.exports = CommentAPI
