require 'whatwg-fetch'
serializer = require '../utils/serializer'

API_URL = '/api/v1'
API_HEADERS = 
  'Content-Type': 'application/json'

CurrentUserAPI =
  fetchCurrentUser:->
    fetch "#{API_URL}/users/current",
      method: 'get'
      credentials: 'include'

  update: (userId, formData)->
    fetch "#{API_URL}/users/#{userId}",
      method: 'put'
      headers: API_HEADERS
      credentials: 'include'
      body: JSON.stringify(serializer.currentUserSerializer(formData))

  loginCurrentUser: (formData)->
    fetch "#{API_URL}/users/login",
      method: 'post'
      headers: API_HEADERS
      credentials: 'include'
      body: JSON.stringify(formData)

  regCurrentUser: (formData)->
    fetch "#{API_URL}/users/register",
      method: 'post'
      headers: API_HEADERS
      credentials: 'include'
      body: JSON.stringify(formData)

  logoutCurrentUser:->
    fetch "#{API_URL}/users/logout",
      method: 'post'
      headers: API_HEADERS
      credentials: 'include'

module.exports = CurrentUserAPI
