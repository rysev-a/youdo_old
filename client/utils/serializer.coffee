serializer =
  addTaskSerializer: (taskData)->
    name:        taskData.name
    price:       taskData.price
    description: taskData.description
    category_id: taskData.categoryId
    customer_id: taskData.customerId

  updateTaskSerializer: (taskData)->
    serializedData = {}

    fields = 
      name: 'name'
      description: 'description'
      status: 'status'
      price: 'price'
      categoryId: 'category_id'
      executorId: 'executor_id'

    for key, value of fields
      if taskData["#{key}"] or taskData["#{key}"] is 0
        serializedData["#{value}"] = taskData["#{key}"]

    return serializedData

  currentUserSerializer: (formData)->
    serializedData = {}

    fields = 
      email: 'email'
      first_name: 'first_name'
      last_name: 'last_name'

    for key, value of fields
      if formData["#{key}"]
        serializedData["#{value}"] = formData["#{key}"]

    serializedData

module.exports = serializer
