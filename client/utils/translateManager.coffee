statusDictionary =
  waiting: 'Выбор исполнителя'
  accepted: 'Выполняется'
  fail: 'Провалено'
  complete: 'Завершено'

translateManager = 
  translateStatus: (status)->
    statusDictionary["#{status}"]

module.exports = translateManager
