AppDispatcher = require '../AppDispatcher'
constants = require '../constants'
TaskAPI = require '../api/TaskAPI'


TaskActionCreators =
  fetchTask: (taskId)->
    AppDispatcher.dispatchAsync TaskAPI.fetchTask(taskId),
      request: constants.FETCH_TASK
      success: constants.FETCH_TASK_SUCCESS
      failure: constants.FETCH_TASK_ERROR

  createTaskDraft: (task)->
    AppDispatcher.dispatch
      type: constants.CREATE_TASK_DRAFT
      payload: {task}

  updateTaskDraft: (field, value)->
    AppDispatcher.dispatch
      type: constants.UPDATE_TASK_DRAFT
      payload: {field, value}

  addTask: (taskData)->
    AppDispatcher.dispatchAsync TaskAPI.addTask(taskData),
      request: constants.ADD_TASK
      success: constants.ADD_TASK_SUCCESS
      failure: constants.ADD_TASK_ERROR

  deleteTask: (taskId, taskIndex)->
    AppDispatcher.dispatchAsync TaskAPI.deleteTask(taskId),
      request: constants.DELETE_TASK
      success: constants.DELETE_TASK_SUCCESS
      failure: constants.DELETE_TASK_ERROR
      {taskIndex}

  updateTask: (taskData)->
    AppDispatcher.dispatchAsync TaskAPI.updateTask(taskData),
      request: constants.UPDATE_TASK
      success: constants.UPDATE_TASK_SUCCESS
      failure: constants.UPDATE_TASK_ERROR

  acceptTask: (taskId, userId)->
    AppDispatcher.dispatchAsync TaskAPI.acceptTask(taskId, userId),
      request: constants.ACCEPT_TASK
      success: constants.ACCEPT_TASK_SUCCESS
      failure: constants.ACCEPT_TASK_ERROR

  refuseTask: (taskId, taskIndex)->
    AppDispatcher.dispatchAsync TaskAPI.updateTask(
          id: taskId, executorId: 0, status: 'waiting'),
      request: constants.REFUSE_TASK
      success: constants.REFUSE_TASK_SUCCESS
      failure: constants.REFUSE_TASK_ERROR
      {taskIndex}

  completeTask: (taskId, taskIndex)->
    AppDispatcher.dispatchAsync TaskAPI.updateTask(id: taskId, status: 'complete'),
      request: constants.COMPLETE_TASK
      success: constants.COMPLETE_TASK_SUCCESS
      failure: constants.COMPLETE_TASK_ERROR
      {taskIndex}

  requestTask: (taskId, userId)->
    AppDispatcher.dispatchAsync TaskAPI.requestTask(taskId, userId),
      request: constants.REQUEST_TASK
      success: constants.REQUEST_TASK_SUCCESS
      failure: constants.REQUEST_TASK_ERROR

  rejectDeclare: (taskId, taskIndex, userId)->
    AppDispatcher.dispatchAsync TaskAPI.rejectDeclare(taskId, userId),
      request: constants.REJECT_DECLARE_TASK
      success: constants.REJECT_DECLARE_TASK_SUCCESS
      failure: constants.REJECT_DECLARE_TASK_ERROR
      {taskIndex}

module.exports = TaskActionCreators
