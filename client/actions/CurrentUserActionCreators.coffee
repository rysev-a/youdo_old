AppDispatcher = require '../AppDispatcher'
CurrentUserAPI = require '../api/CurrentUserAPI'
constants = require '../constants'


currentUserActionCreators =
  logoutCurrentUser: ->
    AppDispatcher.dispatchAsync CurrentUserAPI.logoutCurrentUser(),
      request: constants.LOGOUT_CURRENT_USER
      success: constants.LOGOUT_CURRENT_USER_SUCCESS
      failure: constants.LOGOUT_CURRENT_USER_ERROR

  fetchCurrentUser: ->
    AppDispatcher.dispatchAsync CurrentUserAPI.fetchCurrentUser(),
      request: constants.FETCH_CURRENT_USER
      success: constants.FETCH_CURRENT_USER_SUCCESS
      failure: constants.FETCH_CURRENT_USER_ERROR

module.exports = currentUserActionCreators
