AppDispatcher = require '../AppDispatcher'
constants = require '../constants'
TaskAPI = require '../api/TaskAPI'

TaskPageActionCreators =
  clear: ->
    AppDispatcher.dispatch
      type: constants.CLEAR_TASK_PAGE


  fetch: (taskId)->
    AppDispatcher.dispatchAsync TaskAPI.fetchTask(taskId),
      request: constants.FETCH_TASK_PAGE
      success: constants.FETCH_TASK_PAGE_SUCCESS
      failure: constants.FETCH_TASK_PAGE_ERROR

module.exports = TaskPageActionCreators
