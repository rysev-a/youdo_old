AppDispatcher = require '../AppDispatcher'
SnackBarStore = require '../stores/SnackBarStore'
constants = require '../constants'

snackBarTimeout = {}

SnackBarActionCreators =
  update: (message, type)->
    AppDispatcher.dispatch
      type: constants.UPDATE_SNACKBAR
      payload: {message, type}

  show: (message, type)->
    return
    clearTimeout(snackBarTimeout)

    AppDispatcher.dispatch
      type: constants.SHOW_SNACKBAR

    snackBarTimeout = -> AppDispatcher.dispatch
      type: constants.HIDE_SNACKBAR

    setTimeout(snackBarTimeout, 2000)

  hide: ->
    return
    AppDispatcher.dispatch
      type: constants.HIDE_SNACKBAR

module.exports = SnackBarActionCreators
