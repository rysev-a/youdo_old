AppDispatcher = require '../../AppDispatcher'
CommentAPI = require '../../api/CommentAPI'
constants = require '../../constants'

CommentFormActionCreators =
  updateCommentForm: (field, value)->
    AppDispatcher.dispatch
      type: constants.UPDATE_COMMENT_FORM
      payload: {field, value}

  submitCommentForm: (formData)->
    AppDispatcher.dispatchAsync CommentAPI.addComment(formData),
      request: constants.SUBMIT_COMMENT_FORM
      success: constants.SUBMIT_COMMENT_FORM_SUCCESS
      failure: constants.SUBMIT_COMMENT_FORM_ERROR


module.exports = CommentFormActionCreators
