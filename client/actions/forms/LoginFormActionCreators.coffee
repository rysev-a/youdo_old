AppDispatcher = require '../../AppDispatcher'
CurrentUserAPI = require '../../api/CurrentUserAPI'
constants = require '../../constants'

LoginFormActionCreators =
  createLoginForm: (task)->
    AppDispatcher.dispatch
      type: constants.CREATE_LOGIN_FORM

  updateLoginForm: (field, value)->
    AppDispatcher.dispatch
      type: constants.UPDATE_LOGIN_FORM
      payload: {field, value}

  submitLoginForm: (formData)->
    AppDispatcher.dispatchAsync CurrentUserAPI.loginCurrentUser(formData),
      request: constants.LOGIN_CURRENT_USER
      success: constants.LOGIN_CURRENT_USER_SUCCESS
      failure: constants.LOGIN_CURRENT_USER_ERROR

  resetErrorsLoginForm: ->
    AppDispatcher.dispatch
      type: constants.RESET_ERRORS_LOGIN_FORM

module.exports = LoginFormActionCreators
