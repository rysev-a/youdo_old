AppDispatcher = require '../../AppDispatcher'
CurrentUserAPI = require '../../api/CurrentUserAPI'
constants = require '../../constants'

RegFormActionCreators =
  createRegForm: (task)->
    AppDispatcher.dispatch
      type: constants.CREATE_REG_FORM

  updateRegForm: (field, value)->
    AppDispatcher.dispatch
      type: constants.UPDATE_REG_FORM
      payload: {field, value}

  submitRegForm: (formData)->
    AppDispatcher.dispatchAsync CurrentUserAPI.regCurrentUser(formData),
      request: constants.REG_CURRENT_USER
      success: constants.REG_CURRENT_USER_SUCCESS
      failure: constants.REG_CURRENT_USER_ERROR

  resetErrorsRegForm: ->
    AppDispatcher.dispatch
      type: constants.RESET_ERRORS_REG_FORM

module.exports = RegFormActionCreators
