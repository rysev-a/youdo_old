AppDispatcher = require '../../AppDispatcher'
CurrentUserAPI = require '../../api/CurrentUserAPI'
constants = require '../../constants'

ProfileFormActionCreators =
  createForm: (task)->
    AppDispatcher.dispatch
      type: constants.CREATE_PROFILE_FORM

  update: (field, value)->
    AppDispatcher.dispatch
      type: constants.UPDATE_PROFILE_FORM
      payload: {field, value}

  submit: (userId, formData)->
    AppDispatcher.dispatchAsync CurrentUserAPI.update(userId, formData),
      request: constants.UPDATE_CURRENT_USER
      success: constants.UPDATE_CURRENT_USER_SUCCESS
      failure: constants.UPDATE_CURRENT_USER_ERROR


module.exports = ProfileFormActionCreators
