AppDispatcher = require '../AppDispatcher'
constants = require '../constants'
TaskAPI = require '../api/TaskAPI'


TaskCategoryListActionCreators =
  fetchTaskCategoryList:->
    AppDispatcher.dispatchAsync TaskAPI.fetchTaskCategoryList(),
      request: constants.FETCH_TASK_CATEGORY_LIST
      success: constants.FETCH_TASK_CATEGORY_LIST_SUCCESS
      failure: constants.FETCH_TASK_CATEGORY_LIST_ERROR

module.exports = TaskCategoryListActionCreators
