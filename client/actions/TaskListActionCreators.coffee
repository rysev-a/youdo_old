AppDispatcher = require '../AppDispatcher'
constants = require '../constants'
TaskAPI = require '../api/TaskAPI'

TaskListActionCreators =
  fetchTaskList:->
    AppDispatcher.dispatchAsync TaskAPI.fetchTaskList(),
      request: constants.FETCH_TASK_LIST
      success: constants.FETCH_TASK_LIST_SUCCESS
      failure: constants.FETCH_TASK_LIST_ERROR

module.exports = TaskListActionCreators
