import os
import json
import pytest
from flask_login import login_user, logout_user
from server import create_app
from server.database import db as database
from server.users.models import User

DB_LOCATION = '/home/alex/www/youdo/test.db'

@pytest.fixture(scope='session')
def app():
    app = create_app(config='test_settings')
    return app

@pytest.fixture(scope='session')
def db(app, request):
    if os.path.exists(DB_LOCATION):
        os.unlink(DB_LOCATION)

    database.app = app
    database.create_all()

    def teardown():
        database.drop_all()
        os.unlink(DB_LOCATION)

    request.addfinalizer(teardown)
    return database

@pytest.fixture(scope='function')
def session(db, request):
    session = db.create_scoped_session()
    db.session = session

    def teardown():
        session.remove()

    request.addfinalizer(teardown)
    return session

@pytest.fixture(scope='function')
def user(session, request):
    user = User(email='super@mail.ru', password='pass')
    session.add(user)
    session.commit()
    def teardown():
        user.query.delete()
        session.commit()

    request.addfinalizer(teardown)
    return user

@pytest.fixture(scope='function')
def logged_in_user(user, client, request):
    response = client.post('/api/users/login', 
                            data=json.dumps({'email': user.email,
                                             'password': 'pass'}), 
                            content_type = 'application/json')

    return user
