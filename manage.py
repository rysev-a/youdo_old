from flask_script import Manager, Shell, Server
from flask_migrate import MigrateCommand
from server import create_app, db
from server.users.cli import user_manager, add as add_user
from server.tasks.cli import task_manager, add_category, add_task

from server.users.models import User
from server.tasks.models import TaskCategory, Task

manager = Manager(app=create_app('settings'))

@manager.command
def init_db():
    db.create_all()

@manager.command
def update_db():
    User.query.delete()
    TaskCategory.query.delete()

    for user_name in ['alex', 'vova', 'alina']:
        add_user(user_name)

    for category_name in [
            'Электроника',
            'Бытовые услуги',
            'Ремонт',
            'Перевозки'
        ]:
        add_category(category_name)

    for taskNumber in range(1, 20):
        add_task(taskNumber)

if __name__ == '__main__':
    #manager.add_option("-c", "--config", dest="settings", required=False)
    manager.add_command('db', MigrateCommand)
    manager.add_command('users', user_manager)
    manager.add_command('tasks', task_manager)
    manager.run()
