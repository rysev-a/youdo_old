from server.users.models import User

def test_create_user_instance(session):
    user_data = {'email': 'model@example.com',
                 'phone': '8(922)8788-552',
                 'first_name': 'alexey',
                 'last_name': 'rysev',
                 'password': 'pa$$worD'}

    user = User(**user_data)

    session.add(user)
    session.commit()

    assert user.id is not None
