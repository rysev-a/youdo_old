from server.users.models import User
from flask_login import login_user
import json

def test_get_users_list(session, client):
    for user_name in ['alex', 'yrii', 'alina']:
        new_user = User(first_name=user_name, email="{}@mail.ru".format(user_name))
        session.add(new_user)
    session.commit()

    response = client.get('/api/v1/users')
    assert '200' in response.status
    assert len(response.json) > 0

def test_create_new_user(session, client):

    user_data = {
        'email': 'new@example.com',
        'phone': '8(922)8788-552',
        'first_name': 'alexey',
        'last_name': 'rysev',
        'password': 'pa$$worD'
    }

    response = client.post('/api/v1/users', 
                            data=json.dumps(user_data), 
                            content_type = 'application/json')


    user = User.query.filter(User.email == user_data['email']).one()

    assert (user.email in response.json.get('email') and
            user.id == response.json.get('id'))

def test_get_user(logged_in_user, client):
    response = client.get('/api/v1/users/{}'.format(logged_in_user.id))
    assert response.json.get('id') is logged_in_user.id
    assert response.json.get('email') in logged_in_user.email

def test_update_user(logged_in_user, client):
    update_data = {'email': 'new@mail.ru', 'first_name': 'alexey'}
    response = client.put('/api/v1/users/{}'.format(logged_in_user.id),
                           data=json.dumps(update_data),
                           content_type='application/json')

    response = client.get('/api/v1/users/{}'.format(logged_in_user.id))
    assert response.json.get('email') in update_data.get('email')

def test_delete_user(logged_in_user, client):
    response = client.delete('/api/v1/users/{}'.format(logged_in_user.id))
    assert User.query.filter_by(email=logged_in_user.email).first() is None

def test_login_user(user, client):
    response = client.post('/api/v1/users/login', 
                            data=json.dumps({'email': user.email,
                                             'password': 'pass'}), 
                            content_type = 'application/json')

    assert response.json.get('email') in user.email

def test_current_user(logged_in_user, client):
    response = client.get('/api/v1/users/current')
    assert response.json.get('email') in logged_in_user.email

def test_logout_user(logged_in_user, client):
    response = client.post('/api/v1/users/logout')
    response = client.get('/api/v1/users/current')
    assert '401' in response.status
