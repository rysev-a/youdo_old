# README #

Service for tasks.

### What is this repository for? ###

* Quick start new project
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone repository
* Make virtual enviroment
* Install python modules **pip install -r requirements**
* Config project in settings.py
* Create database **python manage.py init_db**
* Run project **python manage.py runserver**
* Run tests **py.test**
