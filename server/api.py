from flask_restful import Api
from .users.resources import (UserCurrent, UserList, UserItem, 
                              UserLogin, UserLogout, UserRegister)        

from .tasks.resources import (TaskList, TaskItem, AcceptTask,
                              TaskCategoryList, RequestTask, RejectDeclare, CommentList)

api = Api()

#init users api
api.add_resource(UserList, '/api/v1/users')
api.add_resource(UserItem, '/api/v1/users/<int:user_id>')
api.add_resource(UserCurrent, '/api/v1/users/current')
api.add_resource(UserLogin, '/api/v1/users/login')
api.add_resource(UserRegister, '/api/v1/users/register')
api.add_resource(UserLogout, '/api/v1/users/logout')

#init task api
api.add_resource(TaskList, '/api/v1/tasks')
api.add_resource(TaskCategoryList, '/api/v1/task-categories')
api.add_resource(TaskItem, '/api/v1/tasks/<int:task_id>')

api.add_resource(RequestTask, '/api/v1/tasks/request/<int:task_id>')
api.add_resource(RejectDeclare, '/api/v1/tasks/reject/<int:task_id>')
api.add_resource(AcceptTask, '/api/v1/tasks/accept/<int:task_id>')

#init comment api
api.add_resource(CommentList, '/api/v1/comments')
