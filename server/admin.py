from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from .users.models import User
from .tasks.models import Task, TaskCategory, TaskComment
from .database import db


admin = Admin()
admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Task, db.session))
admin.add_view(ModelView(TaskComment, db.session))
admin.add_view(ModelView(TaskCategory, db.session))
