from sqlalchemy.orm import relationship
from sqlalchemy_utils import ChoiceType

from ..database import db
from ..users.models import User

    
declarers = db.Table('declarers',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('task_id', db.Integer, db.ForeignKey('task.id'))
)

class Task(db.Model):
    """main task model"""

    STATUSES = {
        'waiting': 'waiting',
        'requested': 'requested',
        'accepted': 'accepted',
        'rejected': 'rejected',
        'failed': 'failed',
        'complete': 'complete'
    }

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(length=500))
    category_id = db.Column(db.Integer, db.ForeignKey('task_category.id'))
    category = db.relationship('TaskCategory')
    customer_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    customer = db.relationship('User', foreign_keys=[customer_id])    
    executor_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    executor = db.relationship('User', foreign_keys=[executor_id])
    description = db.Column(db.String(length=500))
    price = db.Column(db.Integer, default=0)

    declared = db.relationship('User',
        secondary=declarers,
        backref=db.backref('declarers', lazy='dynamic'),
        lazy='dynamic')

    status = db.Column(db.String(length=10), default='waiting')

    def __str__(self):
        return self.name

class TaskCategory(db.Model):
    """category for task model"""
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(length=500))

    def __str__(self):
        return self.name

class TaskComment(db.Model):
    """comment for task"""
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User')
    task_id = db.Column(db.Integer, db.ForeignKey('task.id'))
    task = db.relationship('Task', backref='comments')
    content = db.Column(db.String(length=1000))
