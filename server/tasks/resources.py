from flask_restful import Resource, reqparse, fields, marshal, marshal_with
from flask import request, jsonify, json

from .models import Task, TaskCategory, TaskComment
from ..users.models import User
from ..bcrypt import bcrypt
from ..database import db
from ..signals import task_requested, task_accepted

user_fields = {
    'id': fields.Integer,
    'email': fields.String,
    'first_name': fields.String
}

comment_fields = {
    'id': fields.Integer,
    'user': fields.Nested(user_fields),
    'content': fields.String
}

task_fields = {
    'id': fields.Integer,
    'price': fields.Integer,
    'name': fields.String,
    'description': fields.String,
    'status': fields.String,
    'customer_id': fields.String,
    'executor_id': fields.String,
    'category': fields.Nested({
            'id': fields.Integer,
            'name': fields.String
        }),
    'declared': fields.List(fields.Nested(user_fields)),
    'comments': fields.List(fields.Nested(comment_fields))
}

#Tasks API
class TaskList(Resource):
    def get(self):
        tasks = Task.query.all()
        return marshal(tasks, task_fields)

    @marshal_with(task_fields)
    def post(self):
        data = request.json
        task = Task(**data)
        db.session.add(task)
        db.session.commit()
        return marshal(task, task_fields)

class TaskItem(Resource):
    def get(self, task_id):
        task = Task.query.get(task_id)
        if not task:
            return {'message': 'not found'}, 400

        task_fields.update({
            'customer': fields.Nested(user_fields)
        })

        return marshal(task, task_fields), 200

    def put(self, task_id):
        data = request.json
        task = Task.query.filter_by(id=task_id)
        task.update(data)
        db.session.commit()
        return marshal(task.one(), task_fields), 200

    def delete(self, task_id):
        task = Task.query.get(task_id)
        task.declared = []
        db.session.delete(task)
        db.session.commit()
        return {'message': 'deletion complete'}, 200

class RequestTask(Resource):
    def post(self, task_id):
        task = Task.query.get(task_id)
        user = User.query.get(request.json.get('userId'))

        task.declared.append(user)
        db.session.commit()

        #signal task request
        task_requested.send(task=task)

        return marshal(task, task_fields), 200

class RejectDeclare(Resource):
    def post(self, task_id):
        task = Task.query.get(task_id)
        user = User.query.get(request.json.get('userId'))

        task.declared.remove(user)
        db.session.commit()

        return marshal(task, task_fields), 200

class AcceptTask(Resource):
    def post(self, task_id):
        task = Task.query.get(task_id)
        task.executor_id = request.json.get('userId')
        task.status = 'accepted'
        task.declared = []
        db.session.commit()

        #signal task accept
        task_accepted.send(task=task)

        return marshal(task, task_fields), 200

task_category_fields = {
    'id': fields.Integer,
    'name': fields.String
}

#Categories API
class TaskCategoryList(Resource):
    def get(self):
        task_categories = TaskCategory.query.all()
        return marshal(task_categories, task_category_fields)


#Comments API
class CommentList(Resource):
    @marshal_with(comment_fields)
    def post(self):
        data = request.json
        comment = TaskComment(**data)
        db.session.add(comment)
        db.session.commit()
        return comment
