from flask_script import Manager
from .. import db
from .models import Task, TaskCategory

task_manager = Manager(usage="Task-based CLI actions.")

@task_manager.command
def add_category(category_name):
    category = TaskCategory(name=category_name)
    db.session.add(category)
    db.session.commit()  

@task_manager.command
def add_task(task_index):
    task = Task(
        name="Задача {}".format(task_index),
        description="Описание задачи {}".format(task_index),
        customer_id=1)
    db.session.add(task)
    db.session.commit()  
