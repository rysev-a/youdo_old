from flask_script import Manager
from .. import db
from .models import User

user_manager = Manager(usage="User-based CLI actions.")

@user_manager.command
def add(user_name):
    
    user = User(first_name=user_name,
                email="{}@mail.ru".format(user_name),
                password="123")

    db.session.add(user)
    db.session.commit()
    
