import re

version_pattern = re.compile(r"/v(?P<version>[0-9a-z\-\+\.]+)", re.IGNORECASE)

class VersionedAPIMiddleware(object):
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        path = environ.get('PATH_INFO', '')
        match = version_pattern.match(path)

        if match:
            environ['API_VERSION'] = match.group(1)
        else:
            environ['API_VERSION'] = None

        return self.app(environ, start_response)


