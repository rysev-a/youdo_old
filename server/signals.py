from blinker import Namespace
from flask_mail import Message
from .mail import mail


youdo_signals = Namespace()
task_requested = youdo_signals.signal('task-requested')
task_accepted = youdo_signals.signal('task-accepted')


def task_requested_handler(farg, **kwargs):
    task = kwargs.get('task')
    msg = Message(subject='Уведомления youdo',
                  body='Новый запрос на выполнение задачи {}'.format(task.name),
                  sender='from@python-dev.ru',
                  recipients=[task.customer.email])
    mail.send(msg)


def task_accepted_handler(farg, **kwargs):
    task  = kwargs.get('task')
    msg = Message(subject='Уведомления youdo',
                  body='Ваш запрос на выполнение задачи {} одобрен'.format(task.name),
                  sender='from@python-dev.ru',
                  recipients=[task.executor.email])
    mail.send(msg)


def connect_handlers():
    task_requested.connect(task_requested_handler)
    task_accepted.connect(task_accepted_handler)
