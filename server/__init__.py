from flask import Flask, render_template
from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy
from flask_login import current_user

from .database import db
from .api import api
from .authorization import login_manager
from .bcrypt import bcrypt
from .middlewares import VersionedAPIMiddleware
from .admin import admin
from .migrate import migrate
from .mail import mail
from .signals import connect_handlers

def create_app(config=None):
    app = Flask(__name__, static_folder='../static')
    app.wsgi_app = VersionedAPIMiddleware(app.wsgi_app)

    if config is not None:
        app.config.from_object(config)

    #init extensions
    db.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)
    api.init_app(app)
    admin.init_app(app)
    migrate.init_app(app, db)
    mail.init_app(app)

    #init blinker signals
    connect_handlers()

    @app.route('/')
    def index():
        return render_template('index.html')

    @app.route('/<path:path>')
    def page(path):
        return render_template('index.html')

    return app


